﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lesson9;

namespace lesson9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your login, it should be less than 20 characters");
            string login = Console.ReadLine();

            Console.WriteLine("Please enter your password,it should be less than 20 characters and" +
                " should contain at least one digit");
            string password = Console.ReadLine();

            Console.WriteLine("Please enter your password again it should match the firs one");
            string confirmPassword = Console.ReadLine();

            try
            {
                var isFit = Login.CheckLogInInfo(login, password, confirmPassword);
                if (isFit)
                {
                    Console.WriteLine("Your have registered");
                }
            }
            catch (WrongLoginException e)
            {
                Console.WriteLine($"Error: {e.Message}");
            }
            catch (WrongPasswordException e)
            {
                Console.WriteLine($"Error: {e.Message}");
            }
        }
    }

}
