﻿using lesson9;

public class Login
{
    public static bool CheckLogInInfo(string login, string password, string confirmPassword)
    {
        if (login.Length >= 20 || login.Contains(" "))
        {
            throw new WrongLoginException("Your login got to be less then 20 characters and should not contain any spaces");
        }
        if (password.Length >= 20 || password.Contains(" ") || !ContainsDigit(password))
        {
            throw new WrongPasswordException("Your password should be less than 20 characters, should not contain any spaces and" +
                " shold have at least one digit");
        }
        if (password != confirmPassword)
        {
            throw new WrongPasswordException("Your passwords didnt match");
        }
        return true;

    }
    private static bool ContainsDigit(string input)
    {
        foreach (char c in input)
        {
            if (Char.IsDigit(c))
            {
                return true;
            }
        }
        return default;
    }
}

